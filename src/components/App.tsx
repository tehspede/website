import React from 'react';

import MainPage from 'components/MainPage';
import AboutPage from 'components/AboutPage';
import ExperiencePage from 'components/ExperiencePage';
import ContactPage from 'components/ContactPage';
import Presentation from 'components/Presentation';

export default () => {
  return (
    <Presentation>
      <MainPage key="/" />
      <AboutPage key="/about" />
      <ExperiencePage key="/experience" />
      <ContactPage key="/contact" />
    </Presentation>
  );
};
