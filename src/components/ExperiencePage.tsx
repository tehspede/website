import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUniversity, faBriefcase } from '@fortawesome/free-solid-svg-icons';

const Container = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

const ListsContainer = styled.div`
  display: flex;

  h4 > * {
    padding: 0 0.25em;
  }

  @media only screen and (max-device-width: 603px) {
   flex-direction: column;
  }
`;

export default () => {
  return (
    <Container>
      <h3>My Experience</h3>
      <ListsContainer>
        <div>
          <h4>
            <FontAwesomeIcon icon={faUniversity} />
            Education
          </h4>
          <ul>
            <li>
            Vaasan Ammattikorkeakoulu
              <ul>
                <li>Bachelor's Degree in Information Technology</li>
                <li>Software Engineering</li>
                <li>September 2014 - May 2018</li>
              </ul>
            </li>
            <li>
            Tampereen lyseon lukio
              <ul>
                <li>International Baccalaureate</li>
                <li>September 2009 - May 2012</li>
              </ul>
            </li>
          </ul>
        </div>
        <div>
          <h4>
            <FontAwesomeIcon icon={faBriefcase} />
            Work
          </h4>
          <ul>
            <li>
            Jubic Oy
              <ul>
                <li>Software Developer</li>
                <li>September 2017 - Present</li>
              </ul>
            </li>
            <li>
            Visma Software Oy
              <ul>
                <li>System Developer</li>
                <li>June 2017 - August 2017</li>
              </ul>
            </li>
            <li>
            Anvia IT-Palvelut Oy
              <ul>
                <li>Software Developer</li>
                <li>June 2016 - August 2016</li>
              </ul>
            </li>
          </ul>
        </div>
      </ListsContainer>
    </Container>
  );
};
