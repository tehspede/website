import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faLocationArrow,
  faPhone,
  faEnvelope
} from '@fortawesome/free-solid-svg-icons';
import { faLinkedin } from '@fortawesome/free-brands-svg-icons';

const Container = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  h3 {
    margin-block-start: 0;
    margin-block-end: 0;
    margin: 0;
  }

  ul {
    margin: 0;
    margin-block-start: 0;
    margin-block-end: 0;
    padding-inline-start: 0;
  }

  li {
    display: inline;
    padding: 0.5em;

    * {
      padding: 0 0.25em;
    }
  }

  p {
    text-align: center;
  }

  @media only screen and (max-device-width: 662px) {
    li {
      display: inherit;
    }
  }
`;

export default () => {
  return (
    <Container>
      <h3>Contact me!</h3>
      <ul>
        <li>
          <FontAwesomeIcon icon={faPhone} />
          <a href="tel:+358409372211">+358 40 9372211</a>
        </li>
        <li>
          <FontAwesomeIcon icon={faEnvelope} />
          <a href="mailto:aleksi@kulta.us">aleksi@kulta.us</a>
        </li>
        <li>
          <FontAwesomeIcon icon={faLocationArrow} />
          <a href="https://goo.gl/maps/vPWhSPemt2MwPXMN7">Vaasa, Finland</a>
        </li>
        <li>
          <FontAwesomeIcon icon={faLinkedin} />
          <a href="https://www.linkedin.com/in/aleksigold">Aleksi Gold</a>
        </li>
      </ul>
      <p>
        This website was built with TypeScript, React, Font Awesome and
        styled&#x2011;components.
      </p>
    </Container>
  );
};
