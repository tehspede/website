import React, { ReactElement, useState, useEffect, useRef } from 'react';
import styled from 'styled-components';

import Slide from 'components//Slide';

import Background from './background.jpg';

interface Props {
  children: ReactElement[];
}

interface OverlayProps {
  enabled?: boolean;
}

const Container = styled.div`
  overflow: hidden;
  height: 100%;
  width: 100vw;
  position: absolute;
  background-image: url(${Background});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

const Overlay = styled.div<OverlayProps>`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100vw;
  transition: background-color .5s ease-in-out;
  background-color: rgba(255, 255, 255, ${({ enabled }) => enabled ? 0.5 : 0});
`;

const getIndex = (children: ReactElement[]) => {
  for (let i = 0; i < children.length; i++) {
    const { key } = children[i];

    if (key && window.location.pathname.endsWith(key.toString())) {
      return i;
    }
  }

  return 0;
};

const createThrottle = (delay: number) => {
  let throttling = false;

  return (callback: Function) => {
    if (throttling) {
      return;
    }

    throttling = true;

    setTimeout(() => {
      throttling = false;
    }, delay);

    callback();
  };
};

export default ({ children }: Props) => {
  const [index, setIndex] = useState<number>(getIndex(children));
  const child = children[index];
  const throttle = useRef<Function>(createThrottle(500)).current;

  useEffect(() => {
    if (!child.key || window.location.pathname.endsWith(child.key.toString())) {
      return;
    }

    window.history.pushState({}, '', `.${child.key}`);
  }, [child.key]);

  useEffect(() => {
    const arrowCallback = ({ keyCode }: KeyboardEvent) => {
      if (keyCode === 40 && index < (children.length - 1)) {
        throttle(() => setIndex(index + 1));
      }

      if (keyCode === 38 && index > 0) {
        throttle(() => setIndex(index - 1));
      }
    };

    const wheelCallback = ({ deltaY }: WheelEvent) => {
      if (deltaY > 0 && index < (children.length - 1)) {
        throttle(() => setIndex(index + 1));
      }

      if (deltaY < 0 && index > 0) {
        throttle(() => setIndex(index - 1));
      }
    };

    let start = 0;

    const touchStartCallback = ({ touches }: TouchEvent) => {
      start = touches[0].clientY;
    };

    const touchEndCallback = ({ changedTouches }: TouchEvent) => {
      const delta = start - changedTouches[0].clientY;

      if (delta > 0 && index < (children.length - 1)) {
        throttle(() => setIndex(index + 1));
      }

      if (delta < 0 && index > 0) {
        throttle(() => setIndex(index - 1));
      }
    };

    document.addEventListener('keydown', arrowCallback);
    document.addEventListener('wheel', wheelCallback);
    document.addEventListener('touchstart', touchStartCallback);
    document.addEventListener('touchend', touchEndCallback);

    return () => {
      document.removeEventListener('keydown', arrowCallback);
      document.removeEventListener('wheel', wheelCallback);
      document.removeEventListener('touchstart', touchStartCallback);
      document.removeEventListener('touchend', touchEndCallback);
    };
  }, [children.length, index, throttle]);

  return (
    <Container>
      <Overlay enabled={index > 0} />
      {children.map((c, i) => (
        <Slide key={`slide-${i}`} after={i > index} before={i < index}>
          {c}
        </Slide>
      ))}
    </Container>
  );
};
