import React from 'react';
import styled from 'styled-components';
import moment from 'moment';

import me from './me.jpg';

const Container = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;

  img {
    width: 10em;
    float: left;
    padding-right: 1em;
  }

  p:first-child {
    float: left;
  }
`;

const birthday = moment('1994-01-21');
const age = moment().diff(birthday, 'years');

export default () => {
  return (
    <Container>
      <h3>About me</h3>
      <div>
        <img alt="Aleksi Gold" src={me} />
        <p>
          I'm a {age}-year old software developer living in Vaasa. I have a
          degree in Information Technology (B.Eng.), from Vaasa University
          Applied Sciences and I majored in Software Engineering. My passion
          lies in solving other people's problems, and in this day and age, they
          can usually be solved with software solutions.
        </p>
        <p>
          My speciality is web development and can I handle both frontend and
          backend parts of it. For frontend, I have used several different
          technologies such as React, TypeScript, Bootstrap and Semantic UI. For
          backend, I have used technologies, such as Java, TypeScript and .NET.
          I have also used other languages for other purposes, such as C and
          Python. I am also fluent in cloud-based stacks such as Azure, AWS and
          OpenShift/Kubernetes. Linux is another strength of mine as I have been
          an avid Linux user for several years at home and work.
        </p>
      </div>
    </Container>
  );
};
