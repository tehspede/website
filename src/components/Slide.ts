import { ReactElement } from 'react';
import styled from 'styled-components';

interface Props {
  after?: boolean;
  before?: boolean;
  children: ReactElement
}

const translateY = ({ before, after }: Props) => {
  if (before) {
    return '-100vh';
  }

  if (after) {
    return '100vh';
  }

  return '0px';
};

const padding = 20;

export default styled.div<Props>`
  position: absolute;
  transition: transform .5s ease-in-out;
  transform: translateY(${translateY});
  height: 100%;
  width: calc(100vw - ${padding * 2}em);
  padding: 0 ${padding}em;

  @media only screen and (max-device-width: 1270px) {
    width: calc(100vw - 20em);
    padding: 0 10em;
  }

  @media only screen and (max-device-width: 950px) {
    width: calc(100vw - 10em);
    padding: 0 5em;
  }

  @media only screen and (max-device-width: 790px) {
    width: calc(100vw - 2em);
    padding: 0 1em;
  }
`;
