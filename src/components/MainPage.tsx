import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export default () => {
  return (
    <Container>
      <h1>Aleksi Gold</h1>
      <h2>Maker | Developer | Engineer</h2>
    </Container>
  );
};
